#!/bin/bash
yum -y install nagios-plugins-bacula nagios-plugins-bonding nagios-plugins-check-updates nagios-plugins-disk nagios-plugins-dns nagios-plugins-http nagios-plugins-icmp nagios-plugins-load nagios-plugins-mysql nagios-plugins-nagios nagios-plugins-nrpe nagios-plugins-ntp nagios-plugins-ping nagios-plugins-procs nagios-plugins-smtp nagios-plugins-tcp nagios-plugins-time nagios-plugins-uptime nagios-plugins-users
sed -i "s/<title>.*<\/title>/<title>${NAGIOS_INSTANCE}<\/title>/" /usr/share/nagios/html/index.php
sed -i "s/<title>.*<\/title>/<title>${NAGIOS_INSTANCE}<\/title>/" /usr/share/nagios/html/side.php
sed -i "s/<title>.*<\/title>/<title>${NAGIOS_INSTANCE}<\/title>/" /usr/share/nagios/html/main.php
sed -i "s/^\$url.*/\$url = 'cgi-bin\/status.cgi?host=all';/" /usr/share/nagios/html/index.php
echo 'RedirectMatch ^/$ /nagios/' >> /etc/httpd/conf.d/nagios.conf
